import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { PlanetsOverviewComponent } from './pages/planets-overview/planets-overview.component';

const routes: Routes = [
  {
    path: '',
    component: PlanetsOverviewComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }

export interface State {
    ID: number
    PSTATABB: string
    FIPSST: number
    STNAMEPCAP: number
    STNGENAN: number
    STGENPERC: number
    YEAR?: string
    STHTIAN?: number
    STHTIOZ?: number
    STHTIANT?: number
    STHTIOZT?: number
    STNGENOZ?: number
    STNOXAN?: number
    STNOXOZ?: number
    STSO2AN?: number
    STCO2AN?: number
    STCH4AN?: number
    STN2OAN?: number
    STCO2EQA?: number
}
import { Planet } from "./Planet.model";
import { State } from "./State.model";

export interface TopNPlanetsOnState {
    planets: Planet[],
    states: State[]
}
import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { PlanetsOverviewComponent } from './pages/planets-overview/planets-overview.component';
import { LeafletModule } from '@asymmetrik/ngx-leaflet';
import { DataSourceModule } from './datasource/data-source.module';
import { FormsModule } from '@angular/forms';

@NgModule({
  declarations: [
    AppComponent,
    PlanetsOverviewComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    LeafletModule,
    DataSourceModule,
    FormsModule 
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }

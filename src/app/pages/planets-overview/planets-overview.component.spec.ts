import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PlanetsOverviewComponent } from './planets-overview.component';

describe('PlanetsOverviewComponent', () => {
  let component: PlanetsOverviewComponent;
  let fixture: ComponentFixture<PlanetsOverviewComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PlanetsOverviewComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(PlanetsOverviewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import * as L from 'leaflet';
import { latLng, marker, tileLayer } from 'leaflet';
import { first } from 'rxjs';
import { StaticDataSource } from 'src/app/datasource/static.datasource';
import { TopNPlanetsDataSource } from 'src/app/datasource/top-n/top-n-planets.datasource';
import { Planet, State } from 'src/app/models';
import Chart, { ChartConfiguration } from 'chart.js/auto'

@Component({
  selector: 'app-planets-overview',
  templateUrl: './planets-overview.component.html',
  styleUrls: ['./planets-overview.component.scss']
})
export class PlanetsOverviewComponent implements OnInit {
  map: L.Map;
  topPlanetsShown: number = 0
  topn = 5
  selectedOverview: OverviewCase = OverviewCase.US;
  netGen = 0
  selectedState = ''
  overviewZoom = 4
  moveAnymation: L.PanOptions
  options: L.MapOptions = {}
  topPlanets: { planet: Planet, marker: L.Marker<any> }[] = [];
  statesOverview: State[] = [];
  stateOverview: State;
  selectedPlanet: Planet;
  overviewCase = OverviewCase
  stChart: Chart;


  constructor(private topPlanetsDataSource: TopNPlanetsDataSource, private staticDataSource: StaticDataSource) {
    this.moveAnymation = {
      animate: true,
      duration: 2,
      easeLinearity: 1,
      noMoveStart: false,
    }
    this.stateOverview = {
      ID: 0,
      PSTATABB: '',
      FIPSST: 0,
      STNAMEPCAP: 0,
      STNGENAN: 0,
      STGENPERC: 0
    }
    this.selectedPlanet = {
      ID: 0,
      LAT: 0,
      LON: 0,
      PNAME: '',
      PSTATABB: '',
      PLNGENAN: 0
    }
  }

  onMapReady(map: L.Map) {
    this.map = map
  }

  ngOnInit(): void {
    this.initMap()
    this.showTopOnUs()
  }


  setMarkersOnMap(topNPlanets: Planet[]) {
    let icon = L.icon({
      iconSize: [40, 60],
      iconAnchor: [20, 30],
      iconUrl: '/assets/icons/power-planet.png',
    })
    this.topPlanets = topNPlanets.map(planet => ({
      planet,
      marker: marker([planet.LAT, planet.LON], { icon: icon }).on('click', () => { this.overviewPowerPlanet(planet) }).bindTooltip(`${planet.PNAME} <br> 
      net generation: ${planet.PLNGENAN} mwh
      `)
    }))

    this.map.fitBounds(topNPlanets.map(planet => [planet.LAT, planet.LON]), { ...this.moveAnymation, maxZoom: 10 })
  }

  showTopOnUs() {
    this.selectedOverview = this.overviewCase.US
    this.selectedState = ''
    this.topPlanetsDataSource.topNOnUnitedStates(this.topn).pipe(first()).subscribe((res) => {
      this.statesOverview = res.states
      this.netGen = res.states.map(state => state.STNGENAN).reduce((acc, curr) => acc + curr)
      this.setMarkersOnMap(res.planets)
      this.drawUsChart(res.states)
    })
  }

  async navigateToState(state: string) {
    if(this.stChart) {
    this.stChart.destroy()
    }
    this.selectedOverview = this.overviewCase.STATE
    this.selectedState = state
    this.topPlanetsDataSource.topNOnState(this.topn, state)
      .subscribe(res => {
        this.setMarkersOnMap(res.planets)
        this.stateOverview = res.state
        this.drawStChart(res.planets)
      })
  }

  search() {
    if (this.selectedState) {
      this.navigateToState(this.selectedState)

    } else {
      this.showTopOnUs()
    }
  }

  overviewPowerPlanet(planet: Planet) {
    this.selectedOverview = OverviewCase.PLANET
    this.map.setView([planet.LAT, planet.LON], 12, this.moveAnymation)
    this.topPlanetsDataSource.getPlanet(planet.ID).subscribe(planet => {
      this.selectedPlanet = planet
    })
  }

  private initMap() {
    let corner1 = L.latLng(17.371214, -211.593791)
    let corner2 = L.latLng(73.143949, -20.168010)
    let bounds = L.latLngBounds(corner1, corner2);
    this.options = {
      layers: [
        tileLayer('http://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', { maxZoom: 18 })
      ],
      maxBounds: bounds,
      zoom: this.overviewZoom,
      center: latLng(39.401234, -96.223023)
    };
  }

  mySortingFunction (a: any, b: any) {
    return a.key > b.key ? -1 : 1;
  }

  drawUsChart(states: State[]) {
    let biggestStates = states.sort((a,b) => {
      return b.STNGENAN - a.STNGENAN
    }).slice(0, 10)
    let chartLabels: string[] =  biggestStates.map(s => s.PSTATABB)
    let dataset: number [] = biggestStates.map(s => s.STNGENAN)
    const data = {
      labels: chartLabels,
      datasets: [{
        data: dataset,
        backgroundColor: [
          'rgba(255, 99, 132, 0.2)',
          'rgba(255, 159, 64, 0.2)',
          'rgba(255, 205, 86, 0.2)',
          'rgba(75, 192, 192, 0.2)',
          'rgba(54, 162, 235, 0.2)',
          'rgba(153, 102, 255, 0.2)',
          'rgba(201, 203, 207, 0.2)'
        ],
   
      }]
    };
    const config:ChartConfiguration = {
      type: 'bar',
      data: data,
      options: {
        scales: {
          y: {
            beginAtZero: true
          }
        }
      },
    };
  
    let ctx:any = document.getElementById('usChart')
    let chart = new Chart(ctx, config)
  }

  drawStChart(planets: Planet[]) {
    let biggestplanets = planets.sort((a,b) => {
      return b.PLNGENAN - a.PLNGENAN
    }).slice(0, 10)
    let chartLabels: string[] =  biggestplanets.map(s => s.PNAME)
    let dataset: number [] = biggestplanets.map(s => s.PLNGENAN)
    const data = {
      labels: chartLabels,
      datasets: [{
        data: dataset,
        backgroundColor: [
          'rgba(255, 99, 132, 0.2)',
          'rgba(255, 159, 64, 0.2)',
          'rgba(255, 205, 86, 0.2)',
          'rgba(75, 192, 192, 0.2)',
          'rgba(54, 162, 235, 0.2)',
          'rgba(153, 102, 255, 0.2)',
          'rgba(201, 203, 207, 0.2)'
        ],
      }]
    };
    const config:ChartConfiguration = {
      type: 'bar',
      data: data,
      options: {
        scales: {
          y: {
            beginAtZero: true
          }
        }
      },
    };
  
    let ctx:any = document.getElementById('stChart')
    this.stChart =  new Chart(ctx, config)
  }

}

enum OverviewCase {
  PLANET,
  STATE,
  US
}



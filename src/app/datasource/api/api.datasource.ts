import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { Observable } from 'rxjs';
import {  map } from 'rxjs/operators';

@Injectable()
export class ApiDatasource {
  constructor(private http: HttpClient) { }

  get(path: string, params: any = new HttpParams()): Observable<any> {
    return this.http.get(path, { params });
  }

}

import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { ApiModule } from './api/api.module';
import { TopNPlanetsDataSource } from './top-n/top-n-planets.datasource';
import { StaticDataSource } from './static.datasource';

@NgModule({
  imports: [HttpClientModule, ApiModule],
  declarations: [],
  providers: [TopNPlanetsDataSource, StaticDataSource]
})
export class DataSourceModule { }

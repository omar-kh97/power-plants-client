import { Injectable } from "@angular/core";
import { map, Observable } from "rxjs";
import { Planet, State } from "src/app/models";
import { ApiDatasource } from "../api/api.datasource";


@Injectable()

export class TopNPlanetsDataSource {
    
    constructor(private api: ApiDatasource){}

    topNOnUnitedStates(n: number):Observable<{planets: Planet[], states: State[]}> {
        return this.api.get('topn', {top: n})
        .pipe(map((res) => ({
            planets: <Planet[]>res.planets,
            states: <State[]>res.states
        })))
    }

    topNOnState(n: number, state: string):Observable<{planets: Planet[], state: State}> {
        return this.api.get(`topn/${state}`, {top: n})
        .pipe(map((res) => ({
            planets: <Planet[]>res.planets,
            state: <State> res.state
        })))
    }

    getPlanet(ID: number): Observable<Planet> {
        return this.api.get(`planet/${ID}`)
        .pipe(map((res) => (<Planet> res)))
      }
}
import { Injectable } from "@angular/core";

@Injectable()
export class StaticDataSource {

    constructor() { }
    statesLoc = [
        { code: "WI", lat: 44.5, lng: -89.5 },
        { code: "WV", lat: 39, lng: -80.5 },
        { code: "VT", lat: 44, lng: -72.699997 },
        { code: "TX", lat: 31, lng: -100 },
        { code: "SD", lat: 44.5, lng: -100 },
        { code: "RI", lat: 41.742325, lng: -71.742332 },
        { code: "OR", lat: 44, lng: -120.5 },
        { code: "NY", lat: 43, lng: -75 },
        { code: "NH", lat: 44, lng: -71.5 },
        { code: "NE", lat: 41.5, lng: -100 },
        { code: "KS", lat: 38.5, lng: -98 },
        { code: "MS", lat: 33, lng: -90 },
        { code: "IL", lat: 40, lng: -89 },
        { code: "DE", lat: 39, lng: -75.5 },
        { code: "CT", lat: 41.599998, lng: -72.699997 },
        { code: "AR", lat: 34.799999, lng: -92.199997 },
        { code: "IN", lat: 40.273502, lng: -86.126976 },
        { code: "MO", lat: 38.573936, lng: -92.60376 },
        { code: "FL", lat: 27.994402, lng: -81.760254 },
        { code: "NV", lat: 39.876019, lng: -117.224121 },
        { code: "ME", lat: 45.367584, lng: -68.972168 },
        { code: "MI", lat: 44.182205, lng: -84.506836 },
        { code: "GA", lat: 33.247875, lng: -83.441162 },
        { code: "HI", lat: 19.741755, lng: -155.844437 },
        { code: "AK", lat: 66.160507, lng: -153.369141 },
        { code: "TN", lat: 35.860119, lng: -86.660156 },
        { code: "VA", lat: 37.926868, lng: -78.024902 },
        { code: "NJ", lat: 39.833851, lng: -74.871826 },
        { code: "KY", lat: 37.839333, lng: -84.27002 },
        { code: "ND", lat: 47.650589, lng: -100.437012 },
        { code: "MN", lat: 46.39241, lng: -94.63623 },
        { code: "OK", lat: 36.084621, lng: -96.921387 },
        { code: "MT", lat: 46.96526, lng: -109.533691 },
        { code: "WA", lat: 47.751076, lng: -120.740135 },
        { code: "UT", lat: 39.41922, lng: -111.950684 },
        { code: "CO", lat: 39.113014, lng: -105.358887 },
        { code: "OH", lat: 40.367474, lng: -82.996216 },
        { code: "AL", lat: 32.31823, lng: -86.902298 },
        { code: "IO", lat: 42.032974, lng: -93.581543 },
        { code: "NM", lat: 34.307144, lng: -106.018066 },
        { code: "SC", lat: 33.836082, lng: -81.163727 },
        { code: "PA", lat: 41.203323, lng: -77.194527 },
        { code: "AZ", lat: 34.048927, lng: -111.093735 },
        { code: "MD", lat: 39.045753, lng: -76.641273 },
        { code: "MA", lat: 42.407211, lng: -71.382439 },
        { code: "CA", lat: 36.778259, lng: -119.417931 },
        { code: "ID", lat: 44.068203, lng: -114.742043 },
        { code: "WY", lat: 43.07597, lng: -107.290283 },
        { code: "NC", lat: 35.782169, lng: -80.793457 },
        { code: "LA", lat: 30.39183, lng: -92.329102 }
    ]

    getStateCoordinates(PSTATABB: string): [number, number] | undefined {
        let selectedState = this.statesLoc.find(state => state.code == PSTATABB)
        if(!selectedState) return undefined 
        else {
            return [selectedState.lat, selectedState.lng]
        }
      }

    public getStatesLog() {
        return this.statesLoc
    }


}


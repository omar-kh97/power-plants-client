export const environment = {
  production: true,
  baseUrl: 'https://power-planet-store.herokuapp.com/api/'
};
